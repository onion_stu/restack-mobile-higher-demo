import chatState from '../common/chats'

import _ from 'lodash'

const findRecord = (list,id) => {
  return _.find(list, function(item){
    return +id == item.recordID
  })
}

const getHigher = state => _.cloneDeep(state.higher)

const makrComment = comment => ({user: 'Me', content: comment, timestamp: new Date().getTime()})

const higher = {
  name: 'higher',
  initialState: {
    total:chatState,
    index:{},
    comments:[],
    userInfo:{}
  },
  sagas: {
    *list(action, {update, select, put, call}) {
      // yield update({isFetching: true})
      const newTotal = yield select((state) => {
        let higherState = _.cloneDeep(state.higher);
        return _.map(higherState.total,(item,index) => {
          if(higherState.index[item.recordID]){
            // console.warn('item.......',item)
            item.conversations = higherState.index[item.recordID]
          }
          return item;
        })
      })
      yield update({total: newTotal})
    },
    *getOne(action, {update, select, put, call}){
      const higherState = yield select(getHigher)
      let result = yield findRecord(higherState.total,action.id);
      // console.warn(result,action)
      let newIndex = Object.assign({},higherState.index)
      if(!newIndex[action.id]){
        newIndex[action.id] = result.conversations
      }
      let comments = newIndex[action.id]
      let userInfo = {
        name: result.name,
        avatar: result.avatar
      }
      yield update({comments:comments,index:newIndex,userInfo:userInfo})
    },
    *fetch() {
      yield console.log("fetching list");
    },
    *update(action, {update, select, put, call}){
      const newComment = makrComment(action.comment)
      const higherState = yield select(getHigher)
      let newComments = [...higherState.comments,newComment]
      let newIndex = Object.assign({},higherState.index)
      newIndex[action.id] = newComments;
      const newTotal = _.map(higherState.total,(item,index) => {
        if(newIndex[item.recordID]){
          item.conversations = newComments
        }
        return item;
      })
      yield update({total:newTotal,comments:newComments,index:newIndex})
    },
    *destroy() {

    }
  }
}

export default higher