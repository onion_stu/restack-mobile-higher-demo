import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import mobiscroll, { Menustrip } from "mobiscroll"
import moment from 'moment'
import classNames from 'classnames'
import {avatarImgs as avatars} from '../common/imgUrls'
import './style/higher.less'
import './style/mobi.less'


@connect(
  state => {
    return {
      conversations: state.higher.total||[]
    }
  }
)
@withRouter
export default class ChatsList extends Component {

  static propTypes = {}

  constructor(props) {
    super(props);
    moment.locale('zh-cn')
  }

  render() {
    const { conversations } = this.props;
    return (
      <div className="conversations">
        <div className="conversations-header">
          <div className="header-body">AirChart</div>
        </div>
        <div className="conversations-search">
          <div className="search-inpiut">
            <i className="icon fa fa-search pull-left"/>
            <i className="icon fa fa-microphone pull-right"/>
          </div>
        </div>
        <div className="conversations-body">
          <div className="conversations-list">
            {
              conversations&&conversations.map((item,index) => {
                let chatText = '',
                    chats = item.conversations,
                    chatLen = chats.length,
                    chat = chats[chatLen-1],
                    timeStr = moment(chat.timestamp).fromNow();
                // console.log(moment(timeStr).fromNow())
                return (
                  <a className="conversation-summary" onClick={this.goDetail} data-href={`/detail/${item.recordID}`} key={`${item.recordID}_${index}`}>
                    <div className="conversation-summary-avatar">
                      <div className="conversation-avatar">
                        <img src={avatars[item.name]}/>
                      </div>
                    </div>
                    <div className="conversation-summary-body">
                      <div className="conversation-summary-meta">
                        <div className="conversation-summary-author">{item.name}</div>
                        <div className="conversation-summary-timestamp">{timeStr}</div>
                      </div>
                      <div className="conversation-summary-body-text">
                        <div className="text-content">{chat.content}</div>
                      </div>
                    </div>
                  </a>
                )
              })
            }
          </div>
        </div>
        <Menustrip
          type="tabs"
          select="single"
          className="mbsc-ms-icons" 
          theme="mobiscroll" 
          display="bottom"
          layout={4}
        >
          <li data-tab="md-tab-chats" data-selected="true"><span className="micons mbsc-ic mbsc-ic-bubbles mbsc-ms-ic">Chats</span></li>
          <li data-tab="md-tab-contacts" data-disabled={true}><span className="micons mbsc-ic mbsc-ic-book mbsc-ms-ic">Contacts</span></li>
          <li data-tab="md-tab-discover" data-disabled={true}><span className="micons mbsc-ic mbsc-ic-earth mbsc-ms-ic">Discover</span></li>
          <li data-tab="md-tab-me" data-disabled={true}><span className="micons mbsc-ic mbsc-ic-user4 mbsc-ms-ic">Me</span></li>
        </Menustrip>
      </div>
    );
  }

  goDetail = (e) => {
    e.preventDefault()
    let el = e.currentTarget
    this.props.router.push(el.dataset.href);
  }

}
