import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import mobiscroll from "mobiscroll"
import moment from 'moment'
import classNames from 'classnames'
import { avatarImgs as avatars} from '../common/imgUrls'
import './style/higher.less'

@connect(
  state => {
    return {
      comments: state.higher.comments,
      userInfo: state.higher.userInfo
    }
  }
)
@withRouter
export default class ChatsDetail extends Component {

  static propTypes = {}

  render() {
    const { userInfo } = this.props
    const { comments, words } = this.state
    let lasttime = 0;
    console.log('....',comments)
    return (
      <div className="conversations conversation">
        <div className="conversations-header">
          <div className="header-buttons">
            <i className="fa fa-angle-left icon pull-left" onClick={this.back}/>
            <i className="fa fa-user icon pull-right"/>
          </div>
          <div className="header-body">{userInfo.name||'Chats'}</div>
        </div>
        <div className="conversations-body">
          <div className="conversation-parts" ref="list">
            {
              comments.map((item,index,list)=>{
                let flag = !!(item.timestamp - lasttime > 1000 * 60 * 5)
                lasttime = item.timestamp
                return this.renderComment(item,index,flag)
              })
            }
          </div>
        </div>
        <div className="conversations-footer">
          <div className="comment-input">
            <i className="icon fa fa-microphone pull-left" />
            {
              words&&words.trim()?
              <div className="btn pull-right" onClick={this.add}>发送</div>
              :
              <i className="icon fa fa-plus-circle pull-right" />
            }
            <div className="input-wapper">
              <input type="text" ref="comment" value={words} onChange={this.onInputChange}/>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderComment = (item,index,isShowTime) => {
    const { userInfo } = this.props
    let flag = item.user==='Me'
    let itemClass = classNames('conversation-part',flag?'conversation-part-user':'conversation-part-other')
    // let logoSrc = flag?'./images/user.jpeg':userInfo.avatar
    let logoSrc = avatars[item.user]
    return (
      <div className={itemClass} key={`${index}`}>
        {
          isShowTime&&
          <div className="conversation-parts-time-divider">{moment(item.timestamp).format('hh:mm')}</div>
        }
        <div className="comment-container">
          <div className="avatar-container">
            <div className="avatar">
              <img src={logoSrc} />
            </div>
          </div>
          <div className="intercom-comment">
            <div className="pre">{item.content}</div>
            <div className="triangle"></div>
          </div>
        </div>
      </div>
    );
  }

  constructor(props) {
    super(props);
    moment.locale('zh-cn')
    const { params:{recordID}, comments, dispatch } = props;
    this.state = {
      comments:comments,
      words:''
    }
    dispatch({
      type:'higher/getOne',
      id:recordID
    })
    // getComments(recordID)
  }

  componentWillReceiveProps(nextProps){
    const { comments } = this.state
    console.log('componentWillReceiveProps..................')
    if(comments.length !== nextProps.comments.length){
      this.setState({
        comments: nextProps.comments
      })
    }
  }

  onInputChange = (event) =>{
    let el = event.currentTarget;
    this.setState({ words: el.value})
  }

  back = () => {
    // console.log(this)
    this.props.router.goBack()
  }

  add = () => {
    const { params:{recordID}, dispatch } = this.props;
    const { comments, words } = this.state
    if(words&&words.trim()){
      // addComment(recordID,words);
      dispatch({
        type: 'higher/update',
        id: recordID,
        comment: words
      })
      this.setState({words:''},() => {
        this.refs.list.scrollIntoView(false);
      })
    }else{
      alert('请输入内容')
    }
  }

}
