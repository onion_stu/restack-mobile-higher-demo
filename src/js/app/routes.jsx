import React from 'react'
import {Route, IndexRoute} from 'react-router'

import App from './App'

import ChatsList from '../higher/Chats'
import Detail from '../higher/Detail'

const rootRoute = (
  <Route path="/" component={App}>
    <IndexRoute component={ChatsList}/>
    <Route path="/detail/:recordID" component={Detail}/>
  </Route>
)

export default rootRoute
