import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import { getPlatform } from '../common/helper'
import classNames from 'classnames'

@connect(
  state => {
    return {
    }
  }
)
@withRouter
export default class App extends Component {

  static propTypes = {

  }

  constructor(props) {
    super(props);
    this.state = getPlatform();
  }

  render() {
    const { isIOS, isAndroid } = this.state;
    const wapperClass = classNames({'ios-platform':isIOS,'android-platform':isAndroid})
    return (
      <div className={wapperClass}>
        {this.props.children}
      </div>
    );
  }
}
