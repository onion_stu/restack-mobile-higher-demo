var chats = [{
  recordID: 1001,
  name: 'Viviane',
  avatar: './images/avatar01.jpeg',
  conversations: [{
    user: 'Viviane',
    content: '你今日饮左未吖？',
    timestamp: 147735956148
  }, {
    user: 'Me',
    content: '啦啦啦~~~',
    timestamp: 1477359938853
  },{
    user: 'Viviane',
    content: 'OK',
    timestamp: 1477362290050
  }]
}, {
  recordID: 1002,
  name: 'Norman',
  avatar: './images/avatar02.jpg',
  conversations: [{
    user: 'Norman',
    content: '你今日饮左未吖？',
    timestamp: 147735956148
  }, {
    user: 'Me',
    content: '啦啦啦~~~',
    timestamp: 1477359938853
  }]
}, {
  recordID: 1003,
  name: 'Jane',
  avatar: './images/avatar03.jpg',
  conversations: [{
    user: 'Jane',
    content: '你今日饮左未吖？',
    timestamp: 147735956148
  }, {
    user: 'Me',
    content: '啦啦啦~~~',
    timestamp: 1477359938853
  }]
}, {
  recordID: 1004,
  name: 'Nina',
  avatar: './images/avatar04.jpg',
  conversations: [{
    user: 'Nina',
    content: '你今日饮左未吖？',
    timestamp: 147735956148
  }, {
    user: 'Me',
    content: '啦啦啦~~~',
    timestamp: 1477359938853
  }]
}, {
  recordID: 1005,
  name: 'Csdsad',
  avatar: './images/avatar05.jpeg',
  conversations: [{
    user: 'Csdsad',
    content: '你今日饮左未吖？',
    timestamp: 147735956148
  }, {
    user: 'Me',
    content: '啦啦啦~~~',
    timestamp: 1477359938853
  }]
}]

module.exports = chats;