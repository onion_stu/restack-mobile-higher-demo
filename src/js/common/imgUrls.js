import user from 'file!../../images/user.jpeg'
import avatar01 from 'file!../../images/avatar01.jpeg'
import avatar02 from 'file!../../images/avatar02.jpeg'
import avatar03 from 'file!../../images/avatar03.jpeg'
import avatar04 from 'file!../../images/avatar04.jpeg'
import avatar05 from 'file!../../images/avatar05.jpeg'


export const avatars = {
  Me: 'http://p1.qqyou.com/touxiang/UploadPic/2015-6/19/2015061914564960842.jpeg',
  Viviane:'http://p1.qqyou.com/touxiang/UploadPic/2015-6/19/2015061914564977613.jpeg',
  Norman:'http://p1.qqyou.com/touxiang/UploadPic/2015-6/19/2015061914565028932.jpeg',
  Jane:'http://p1.qqyou.com/touxiang/UploadPic/2015-6/19/2015061914565132321.jpeg',
  Nina:'http://p1.qqyou.com/touxiang/UploadPic/2015-6/19/2015061914565043612.jpeg',
  Csdsad:'http://p1.qqyou.com/touxiang/UploadPic/2015-6/19/2015061914565041688.jpeg',
}

export const avatarImgs = {
  Me: user,
  Viviane:avatar01,
  Norman:avatar02,
  Jane:avatar03,
  Nina:avatar04,
  Csdsad:avatar05,
}